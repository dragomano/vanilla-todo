export default function setupTodoList(root, title) {
  let tasks = [];

  async function fetchTasks() {
    await fetch('https://dummyapi.online/api/todos')
      .then((response) => response.json())
      .then((data) => {
        tasks = data.slice(0, 10);
        renderTasks();
      });
  }

  function addTask() {
    const input = root.querySelector('input[type=text]');
    const title = input.value.trim();

    if (title) {
      const task = {
        id: Date.now(),
        title: title,
        completed: false,
      };

      tasks.push(task);
      input.value = '';
      renderTask(task);
    }

    input.focus();
  }

  window.addTask = addTask;

  function toggleTask(id) {
    const index = tasks.findIndex((task) => task.id === id);

    if (index !== -1) {
      tasks[index].completed = !tasks[index].completed;

      const taskElement = root.querySelector(`[data-id="${id}"]`);

      if (taskElement) {
        const checkbox = taskElement.querySelector('input[type="checkbox"]');
        const span = taskElement.querySelector('span');

        checkbox.checked = tasks[index].completed;
        span.classList.toggle('line-through', tasks[index].completed);
      }
    }
  }

  window.toggleTask = toggleTask;

  function deleteTask(id) {
    const index = tasks.findIndex((task) => task.id === id);

    if (index !== -1) {
      tasks.splice(index, 1);

      // Находим элемент, содержащий задачу с нужным id
      const taskElement = root.querySelector(`[data-id="${id}"]`);

      // Если элемент найден, удаляем его из DOM
      if (taskElement) {
        taskElement.remove();
      }
    }
  }

  window.deleteTask = deleteTask;

  function renderTasks() {
    root.innerHTML = `
      <div class="max-w-sm md:max-w-lg mx-auto my-10 bg-white rounded-md shadow-md overflow-hidden">
        <h1 class="text-2xl font-bold text-center py-4 bg-gray-100">${title}</h1>
        <ul class="list-none p-4"></ul>
        <div class="p-4 bg-gray-100">
          <div class="flex items-center">
            <input type="text" class="flex-1 mr-2 py-2 px-4 rounded-md border border-gray-300" placeholder="Новая задача" autofocus>
            <button type="submit" class="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md" onclick="addTask()">
              Добавить
            </button>
          </div>
        </div>
      </div>
    `;

    tasks.forEach((task) => renderTask(task));
  }

  function renderTask(task) {
    const taskHTML = `
      <li class="flex items-center mb-2 hover:cursor-pointer" data-id="${task.id}" onclick="toggleTask(${task.id})">
        <input type="checkbox" class="mr-2" ${task.completed ? 'checked' : ''}>
        <span ${task.completed ? 'class="line-through"' : ''}>${task.title}</span>
        <div class="ml-auto">
          <button class="text-gray-400 hover:text-gray-600" onclick="deleteTask(${task.id})">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
              <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
            </svg>
          </button>
        </div>
      </li>
    `;

    // Сохраняем элемент списка в переменную list
    const list = root.querySelector('ul');

    // Добавляем задачу в список
    list.insertAdjacentHTML('beforeend', taskHTML);
  }

  window.onload = fetchTasks;
}